// -- user code here --

/* --- start generated code --- */

// Generated by  1.5.4 (Phaser v2.6.2)
/**
 * ErrorPanel.
 * 
 * @param {Phaser.Game}
 *            aGame A reference to the currently running game.
 * @param {Phaser.Group}
 *            aParent The parent Group (or other {@link DisplayObject}) that
 *            this group will be added to. If undefined/unspecified the Group
 *            will be added to the {@link Phaser.Game#world Game World}; if
 *            null the Group will not be added to any parent.
 * @param {string}
 *            aName A name for this group. Not used internally but useful for
 *            debugging.
 * @param {boolean}
 *            aAddToStage If true this group will be added directly to the
 *            Game.Stage instead of Game.World.
 * @param {boolean}
 *            aEnableBody If true all Sprites created with {@link #create} or
 *            {@link #createMulitple} will have a physics body created on them.
 *            Change the body type with {@link #physicsBodyType}.
 * @param {number}
 *            aPhysicsBodyType The physics body type to use when physics bodies
 *            are automatically added. See {@link #physicsBodyType} for values.
 */
function ErrorPanel(aGame, aParent, aName, aAddToStage, aEnableBody, aPhysicsBodyType) {

	Phaser.Group.call(this, aGame, aParent, aName, aAddToStage, aEnableBody, aPhysicsBodyType);
	var _bg = this.game.add.button(640.0, 360.0, 'bg_blank', null, this, null, null, null, null, this);
	_bg.alpha = 0.8;
	_bg.anchor.set(0.5, 0.5);

	var _container = this.game.add.group(this);
	_container.position.set(640.0, 360.0);

	var _panel = this.game.add.sprite(0.0, 0.0, 'ui_011', 'error_panel', _container);
	_panel.anchor.set(0.5, 0.5);

	var _header = this.game.add.text(-245.0, -77.0, 'Message', {
		"font" : "16px Kanit",
		"fill" : "#ffffff",
		"align" : "center"
	}, _container);
	_header.anchor.set(0.5, 0.5);

	var _message = this.game.add.text(0.0, 24.0, 'Login fail', {
		"font" : "32px Kanit",
		"fill" : "#ffffff",
		"align" : "center"
	}, _container);
	_message.anchor.set(0.5, 0.5);

	var _btnGroup = this.game.add.group(_container);
	_btnGroup.position.set(0.0, 75.0);

	var _btn = this.game.add.button(0.0, 0.0, 'ui_011', this.onClick, this, null, 'btn_close_error_panel', null, null, _btnGroup);
	_btn.anchor.set(0.5, 0.5);

	var _textBtn = this.game.add.text(0.0, 0.0, 'OK', {
		"font" : "24px Kanit",
		"fill" : "#2e2e2d",
		"align" : "center"
	}, _btnGroup);
	_textBtn.anchor.set(0.5, 0.5);

	// fields

	this.fBg = _bg;
	this.fContainer = _container;
	this.fHeader = _header;
	this.fMessage = _message;
	this.fBtnGroup = _btnGroup;
	this.fBtn = _btn;
	this.fTextBtn = _textBtn;

	this.afterCreate();

}

/** @type Phaser.Group */
var ErrorPanel_proto = Object.create(Phaser.Group.prototype);
ErrorPanel.prototype = ErrorPanel_proto;
ErrorPanel.prototype.constructor = ErrorPanel;

/* --- end generated code --- */
// -- user code here --
ErrorPanel.prototype.afterCreate = function() {
	this.isExit = false;

	this.fBtnGroup.visible = false;

	this.updateLanguage();

	this.fContainer.scale.set(0, 0);
	this.fBg.alpha = 0;
};

ErrorPanel.prototype.show = function(message, isExit, isReconnect) {

	this.isReconnect = false;
	if (isReconnect != 'undefined') {
		this.isReconnect = isReconnect;
	}

	this.isExit = isExit;
	this.fMessage.text = message;

	sound.play(sound.SFX.CANCEL_1);

	this.game.add.tween(this.fContainer.scale).to({
		x : 1,
		y : 1
	}, 250, Phaser.Easing.Back.Out, true);

	this.game.add.tween(this.fBg).to({
		alpha : 1
	}, 250, Phaser.Easing.Cubic.Out, true).onComplete.add(this.onShowFinished, this);
};

ErrorPanel.prototype.onShowFinished = function() {
	if (this.isReconnect == true) {
		this.twRecon = this.game.time.events.add(15000, this.lastMsg, this);
	} else {
		this.game.time.events.add(2000, this.hide, this);
	}
};

ErrorPanel.prototype.stopRecon = function() {
	if (this.twRecon != null) {
		this.twRecon.stop();
		this.twRecon = null;
	}

	this.destroy();
};

ErrorPanel.prototype.lastMsg = function() {
	this.fBtnGroup.visible = true;
	this.fMessage.text = getLang().DISCONNECTED;
	// this.game.state.getCurrentState().showLastMsg();
	// this.game.state.getCurrentState().exit();
};

ErrorPanel.prototype.exitGame = function() {
	this.game.state.getCurrentState().exit();
};

ErrorPanel.prototype.onClick = function() {
	this.game.state.getCurrentState().exit();
	// if (!this.isExit) {
	// this.hide();
	// } else {
	// this.game.currentState.exit();
	// }
};

ErrorPanel.prototype.hide = function() {
	this.game.add.tween(this.fContainer.scale).to({
		x : 0,
		y : 0
	}, 250, Phaser.Easing.Back.In, true);

	this.game.add.tween(this.fBg).to({
		alpha : 0
	}, 250, Phaser.Easing.Cubic.Out, true).onComplete.add(this.onHideFinished, this);
};

ErrorPanel.prototype.onHideFinished = function() {
	if (!this.isExit) {
		this.destroy();
	} else {
		this.destroy();
		this.game.currentState.exit();
	}
};

ErrorPanel.prototype.updateLanguage = function() {
	this.fHeader.text = getLang().ERROR_PANEL_HEADER;
	this.fTextBtn.text = getLang().ERROR_PANEL_BTN;
};
