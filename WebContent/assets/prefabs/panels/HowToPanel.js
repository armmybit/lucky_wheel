// -- user code here --

/* --- start generated code --- */

// Generated by  1.5.4 (Phaser v2.6.2)
/**
 * HowToPanel.
 * 
 * @param {Phaser.Game}
 *            aGame A reference to the currently running game.
 * @param {Phaser.Group}
 *            aParent The parent Group (or other {@link DisplayObject}) that
 *            this group will be added to. If undefined/unspecified the Group
 *            will be added to the {@link Phaser.Game#world Game World}; if
 *            null the Group will not be added to any parent.
 * @param {string}
 *            aName A name for this group. Not used internally but useful for
 *            debugging.
 * @param {boolean}
 *            aAddToStage If true this group will be added directly to the
 *            Game.Stage instead of Game.World.
 * @param {boolean}
 *            aEnableBody If true all Sprites created with {@link #create} or
 *            {@link #createMulitple} will have a physics body created on them.
 *            Change the body type with {@link #physicsBodyType}.
 * @param {number}
 *            aPhysicsBodyType The physics body type to use when physics bodies
 *            are automatically added. See {@link #physicsBodyType} for values.
 */
function HowToPanel(aGame, aParent, aName, aAddToStage, aEnableBody, aPhysicsBodyType) {

	Phaser.Group.call(this, aGame, aParent, aName, aAddToStage, aEnableBody, aPhysicsBodyType);
	var _container = this.game.add.group(this);
	_container.position.set(640.0, 360.0);

	var _bg_win = this.game.add.button(0.0, 0.0, 'bg_win1', this.onClose, this, null, null, null, null, _container);
	_bg_win.anchor.set(0.5, 0.5);

	var _btnR = this.game.add.button(325.0, 0.0, 'ui_011', this.next, this, null, 'btn_change_language', null, null, _container);
	_btnR.anchor.set(0.5, 0.5);

	var _btnL = this.game.add.button(-325.0, 0.0, 'ui_011', this.back, this, null, 'btn_change_language', null, null, _container);
	_btnL.scale.set(-1.0, 1.0);
	_btnL.anchor.set(0.5, 0.5);

	var _indexGroup = this.game.add.group(_container);
	_indexGroup.position.set(0.0, 125.0);

	var _point_index = this.game.add.sprite(-40.0, 0.0, 'ui_011', 'point_index', _indexGroup);
	_point_index.scale.set(0.6, 0.6);
	_point_index.anchor.set(0.5, 0.5);

	var _point_index1 = this.game.add.sprite(0.0, 0.0, 'ui_011', 'point_index', _indexGroup);
	_point_index1.scale.set(0.6, 0.6);
	_point_index1.anchor.set(0.5, 0.5);

	var _point_index2 = this.game.add.sprite(40.0, 0.0, 'ui_011', 'point_index', _indexGroup);
	_point_index2.scale.set(0.6, 0.6);
	_point_index2.anchor.set(0.5, 0.5);

	var _imgGroup = this.game.add.group(_container);

	var _how_to_ = this.game.add.sprite(0.0, 0.0, 'ui_03', 'how_to_1', _imgGroup);
	_how_to_.anchor.set(0.5, 0.5);

	var _how_to_1 = this.game.add.sprite(0.0, 0.0, 'ui_03', 'how_to_2', _imgGroup);
	_how_to_1.anchor.set(0.5, 0.5);

	var _how_to_2 = this.game.add.sprite(0.0, 0.0, 'ui_03', 'how_to_3', _imgGroup);
	_how_to_2.anchor.set(0.5, 0.5);

	var _header = this.game.add.text(0.0, -155.0, 'วิธีการเล่น', {
		"font" : "bold 36px Kanit",
		"fill" : "#ffffff",
		"strokeThickness" : 4,
		"align" : "center"
	}, _container);
	_header.anchor.set(0.5, 0.5);

	var _message = this.game.add.text(0.0, 168.0, 'เลือกเหรียญชิป', {
		"font" : "32px Kanit",
		"fill" : "#ffffff",
		"strokeThickness" : 6,
		"align" : "center"
	}, _container);
	_message.anchor.set(0.5, 0.5);

	// fields

	this.fBtnR = _btnR;
	this.fBtnL = _btnL;
	this.fIndexGroup = _indexGroup;
	this.fImgGroup = _imgGroup;
	this.fHeader = _header;
	this.fMessage = _message;

	this.afterCreate();

}

/** @type Phaser.Group */
var HowToPanel_proto = Object.create(Phaser.Group.prototype);
HowToPanel.prototype = HowToPanel_proto;
HowToPanel.prototype.constructor = HowToPanel;

/* --- end generated code --- */
// -- user code here --
HowToPanel.prototype.afterCreate = function() {
	this.images = this.fImgGroup.children;
	this.indexPoints = this.fIndexGroup.children;

	this.index = 0;
	for (var i = 1; i < this.images.length; i++) {
		this.images[i].visible = false;
		this.indexPoints[i].tint = 0x000000;
	}

	this.updateLanguage();
	this.updateBtn();
};

HowToPanel.prototype.next = function() {
	if (this.index >= this.images.length - 1) {
		return;
	}

	sound.play(sound.SFX.BUTTON_1);

	this.index++;

	for (var i = 0; i < this.images.length; i++) {
		if (i == this.index) {
			this.images[i].visible = true;
		} else {
			this.images[i].visible = false;
		}
	}

	this.updateLanguage();
	this.updateBtn();
};

HowToPanel.prototype.back = function() {
	if (this.index <= 0) {
		return;
	}

	sound.play(sound.SFX.BUTTON_1);

	this.index--;

	for (var i = 0; i < this.images.length; i++) {
		if (i == this.index) {
			this.images[i].visible = true;
		} else {
			this.images[i].visible = false;
		}
	}

	this.updateLanguage();
	this.updateBtn();
};

HowToPanel.prototype.updateBtn = function() {

	this.fBtnL.tint = 0xffffff;
	this.fBtnR.tint = 0xffffff;

	if (this.index <= 0) {
		this.fBtnL.tint = 0x111111;
	}

	if (this.index >= this.images.length - 1) {
		this.fBtnR.tint = 0x111111;
	}

	for (var i = 0; i < this.indexPoints.length; i++) {
		this.indexPoints[i].tint = 0x000000;
	}
	this.indexPoints[this.index].tint = 0xffffff;

};

HowToPanel.prototype.updateLanguage = function() {
	this.fHeader.text = getLang().HOW_TO_HEADER;

	switch (this.index) {
	case 0:
		this.fMessage.text = getLang().HOW_TO_STEP_1;
		break;
	case 1:
		this.fMessage.text = getLang().HOW_TO_STEP_2;
		break;
	case 2:
		this.fMessage.text = getLang().HOW_TO_STEP_3;
		break;
	}

};

HowToPanel.prototype.onClose = function() {
	sound.play(sound.SFX.BUTTON_BACK_1);
	this.destroy();
};