// -- user code here --

/* --- start generated code --- */

// Generated by  1.5.4 (Phaser v2.6.2)


/**
 * WinRatesPanel.
 * @param {Phaser.Game} aGame A reference to the currently running game.
 * @param {Phaser.Group} aParent The parent Group (or other {@link DisplayObject}) that this group will be added to.    If undefined/unspecified the Group will be added to the {@link Phaser.Game#world Game World}; if null the Group will not be added to any parent.
 * @param {string} aName A name for this group. Not used internally but useful for debugging.
 * @param {boolean} aAddToStage If true this group will be added directly to the Game.Stage instead of Game.World.
 * @param {boolean} aEnableBody If true all Sprites created with {@link #create} or {@link #createMulitple} will have a physics body created on them. Change the body type with {@link #physicsBodyType}.
 * @param {number} aPhysicsBodyType The physics body type to use when physics bodies are automatically added. See {@link #physicsBodyType} for values.
 */
function WinRatesPanel(aGame, aParent, aName, aAddToStage, aEnableBody, aPhysicsBodyType) {
	
	Phaser.Group.call(this, aGame, aParent, aName, aAddToStage, aEnableBody, aPhysicsBodyType);
	this.game.add.sprite(992.0, 566.0, 'ui_011', 'box_panel', this);
	
	this.game.add.sprite(992.0, 528.0, 'ui_011', 'panel_header', this);
	
	var _header = this.game.add.text(1006.0, 537.0, 'Win Rates', {"font":"20px Kanit","fill":"#f0e68c"}, this);
	
	var _luckyGroup = this.game.add.group(this);
	_luckyGroup.position.set(5.0, 2.0);
	
	this.game.add.text(1016.0, 574.0, 'L', {"font":"bold 20px Kanit","fill":"#f0e68c"}, _luckyGroup);
	
	var _lRate = this.game.add.text(1109.0, 575.0, '100%', {"font":"20px Kanit","fill":"#f0e68c","align":"right"}, _luckyGroup);
	_lRate.anchor.set(1.0, 0.0);
	
	this.game.add.text(1016.0, 601.0, 'U', {"font":"bold 20px Kanit","fill":"#f0e68c"}, _luckyGroup);
	
	var _uRate = this.game.add.text(1109.0, 602.0, '100%', {"font":"20px Kanit","fill":"#f0e68c","align":"right"}, _luckyGroup);
	_uRate.anchor.set(1.0, 0.0);
	
	this.game.add.text(1016.0, 628.0, 'C', {"font":"bold 20px Kanit","fill":"#f0e68c"}, _luckyGroup);
	
	var _cRate = this.game.add.text(1109.0, 629.0, '100%', {"font":"20px Kanit","fill":"#f0e68c","align":"right"}, _luckyGroup);
	_cRate.anchor.set(1.0, 0.0);
	
	this.game.add.text(1016.0, 655.0, 'K', {"font":"bold 20px Kanit","fill":"#f0e68c"}, _luckyGroup);
	
	var _kRate = this.game.add.text(1109.0, 656.0, '100%', {"font":"20px Kanit","fill":"#f0e68c","align":"right"}, _luckyGroup);
	_kRate.anchor.set(1.0, 0.0);
	
	this.game.add.text(1016.0, 682.0, 'Y', {"font":"bold 20px Kanit","fill":"#f0e68c"}, _luckyGroup);
	
	var _yRate = this.game.add.text(1109.0, 683.0, '100%', {"font":"20px Kanit","fill":"#f0e68c","align":"right"}, _luckyGroup);
	_yRate.anchor.set(1.0, 0.0);
	
	var _gamesGroup = this.game.add.group(this);
	_gamesGroup.position.set(150.0, 5.0);
	
	this.game.add.text(1016.0, 574.0, 'G', {"font":"bold 20px Kanit","fill":"#f0e68c"}, _gamesGroup);
	
	var _gRate = this.game.add.text(1109.0, 575.0, '100%', {"font":"20px Kanit","fill":"#f0e68c","align":"right"}, _gamesGroup);
	_gRate.anchor.set(1.0, 0.0);
	
	this.game.add.text(1016.0, 601.0, 'A', {"font":"bold 20px Kanit","fill":"#f0e68c"}, _gamesGroup);
	
	var _aRate = this.game.add.text(1109.0, 602.0, '100%', {"font":"20px Kanit","fill":"#f0e68c","align":"right"}, _gamesGroup);
	_aRate.anchor.set(1.0, 0.0);
	
	this.game.add.text(1016.0, 628.0, 'M', {"font":"bold 20px Kanit","fill":"#f0e68c"}, _gamesGroup);
	
	var _mRate = this.game.add.text(1109.0, 629.0, '100%', {"font":"20px Kanit","fill":"#f0e68c","align":"right"}, _gamesGroup);
	_mRate.anchor.set(1.0, 0.0);
	
	this.game.add.text(1016.0, 655.0, 'E', {"font":"bold 20px Kanit","fill":"#f0e68c"}, _gamesGroup);
	
	var _eRate = this.game.add.text(1109.0, 656.0, '100%', {"font":"20px Kanit","fill":"#f0e68c","align":"right"}, _gamesGroup);
	_eRate.anchor.set(1.0, 0.0);
	
	this.game.add.text(1016.0, 682.0, 'S', {"font":"bold 20px Kanit","fill":"#f0e68c"}, _gamesGroup);
	
	var _sRate = this.game.add.text(1109.0, 683.0, '100%', {"font":"20px Kanit","fill":"#f0e68c","align":"right"}, _gamesGroup);
	_sRate.anchor.set(1.0, 0.0);
	
	
	
	// fields
	
	this.fHeader = _header;
	this.fLRate = _lRate;
	this.fURate = _uRate;
	this.fCRate = _cRate;
	this.fKRate = _kRate;
	this.fYRate = _yRate;
	this.fGRate = _gRate;
	this.fARate = _aRate;
	this.fMRate = _mRate;
	this.fERate = _eRate;
	this.fSRate = _sRate;
	
	this.afterCreate();
	
}

/** @type Phaser.Group */
var WinRatesPanel_proto = Object.create(Phaser.Group.prototype);
WinRatesPanel.prototype = WinRatesPanel_proto;
WinRatesPanel.prototype.constructor = WinRatesPanel;

/* --- end generated code --- */
// -- user code here --
WinRatesPanel.prototype.afterCreate = function() {
	this.labels = [ this.fLRate, this.fURate, this.fCRate, this.fKRate, this.fYRate, this.fGRate, this.fARate, this.fMRate, this.fERate, this.fSRate ];
	this.chars = [ 'L', 'U', 'C', 'K', 'Y', 'G', 'A', 'M', 'E', 'S' ];

	this.updateLanguage();
};

WinRatesPanel.prototype.setInfo = function(l, u, c, k, y, g, a, m, e, s) {
	this.fLRate.text = l + "%";
	this.fURate.text = u + "%";
	this.fCRate.text = c + "%";
	this.fKRate.text = k + "%";
	this.fYRate.text = y + "%";
	this.fGRate.text = g + "%";
	this.fARate.text = a + "%";
	this.fMRate.text = m + "%";
	this.fERate.text = e + "%";
	this.fSRate.text = s + "%";
};

WinRatesPanel.prototype.setWinRates = function(rates) {
	for (var i = 0; i < rates.length; i++) {
		this.labels[i].text = rates[i].rate + "%";
	}
};

WinRatesPanel.prototype.updateLanguage = function() {
	this.fHeader.text = getLang().WIN_RATES_PANEL_HEADER;
};