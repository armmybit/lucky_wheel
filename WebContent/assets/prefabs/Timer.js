// -- user code here --

/* --- start generated code --- */

// Generated by  1.5.4 (Phaser v2.6.2)


/**
 * Timer.
 * @param {Phaser.Game} aGame A reference to the currently running game.
 * @param {Phaser.Group} aParent The parent Group (or other {@link DisplayObject}) that this group will be added to.    If undefined/unspecified the Group will be added to the {@link Phaser.Game#world Game World}; if null the Group will not be added to any parent.
 * @param {string} aName A name for this group. Not used internally but useful for debugging.
 * @param {boolean} aAddToStage If true this group will be added directly to the Game.Stage instead of Game.World.
 * @param {boolean} aEnableBody If true all Sprites created with {@link #create} or {@link #createMulitple} will have a physics body created on them. Change the body type with {@link #physicsBodyType}.
 * @param {number} aPhysicsBodyType The physics body type to use when physics bodies are automatically added. See {@link #physicsBodyType} for values.
 */
function Timer(aGame, aParent, aName, aAddToStage, aEnableBody, aPhysicsBodyType) {
	
	Phaser.Group.call(this, aGame, aParent, aName, aAddToStage, aEnableBody, aPhysicsBodyType);
	var _container = this.game.add.group(this);
	
	var _bgBlack = this.game.add.sprite(0.0, 0.0, 'ui_011', 'timer', _container);
	_bgBlack.alpha = 0.7;
	_bgBlack.anchor.set(0.5, 0.5);
	
	var _bgWhite = this.game.add.sprite(0.0, 0.0, 'ui_011', 'timer_wh', _container);
	_bgWhite.alpha = 0.0;
	_bgWhite.anchor.set(0.5, 0.5);
	
	var _time = this.game.add.text(0.0, -11.0, '30', {"font":"60px Kanit","fill":"#ffffff","align":"center"}, _container);
	_time.anchor.set(0.5, 0.5);
	
	var _state = this.game.add.text(0.0, 74.0, 'เดิมพันได้', {"font":"32px Kanit","fill":"#ffffff","strokeThickness":4,"align":"center"}, _container);
	_state.pivot.set(0.0, 41.0);
	_state.anchor.set(0.5, 0.5);
	
	var _spinText = this.game.add.text(0.0, 0.0, 'กำลังหมุน...', {"font":"32px Kanit","fill":"#ffffff","strokeThickness":4,"align":"center"}, this);
	_spinText.scale.set(0.0, 0.0);
	_spinText.anchor.set(0.5, 0.5);
	
	
	
	// fields
	
	this.fContainer = _container;
	this.fBgBlack = _bgBlack;
	this.fBgWhite = _bgWhite;
	this.fTime = _time;
	this.fState = _state;
	this.fSpinText = _spinText;
	
	this.afterCreate();
	
}

/** @type Phaser.Group */
var Timer_proto = Object.create(Phaser.Group.prototype);
Timer.prototype = Timer_proto;
Timer.prototype.constructor = Timer;

/* --- end generated code --- */
// -- user code here --
Timer.prototype.afterCreate = function() {
	this.isHide = false;
	this.fSpinText.text = getLang().TIME_STATE_SPIN;
};

Timer.prototype.updateTime = function(time, state) {

	this.fTime.text = time;
	this.lastState = state;

	switch (state) {
	case 'BETTING':
		// this.TIME_STATE_BET = "Bet";
		// this.TIME_STATE_WAIT = "Waiting";
		// this.TIME_STATE_SPIN = "Spinning";
		this.fState.text = getLang().TIME_STATE_BET;
		this.fBgBlack.alpha = 1;
		this.fBgWhite.alpha = 0;
		this.fTime.fill = '#ffffff';
		if (time == 1) {
			this.hide();
		} else if (this.isHide) {
			this.show();
		}
		break;
	case 'WAITING':
		this.fState.text = getLang().TIME_STATE_WAIT;
		this.fBgBlack.alpha = 0;
		this.fBgWhite.alpha = 1;
		this.fTime.fill = '#000000';
		break;
	}
};

Timer.prototype.hide = function() {
	this.isHide = true;

	this.game.add.tween(this.fContainer.scale).to({
		x : 0,
		y : 0
	}, 500, Phaser.Easing.Back.In, true).onComplete.add(this.showSpinningText, this);
};

Timer.prototype.show = function() {
	this.isHide = false;
	this.fSpinText.scale.set(0, 0);

	this.game.add.tween(this.fContainer.scale).to({
		x : 1,
		y : 1
	}, 500, Phaser.Easing.Back.Out, true);
};

Timer.prototype.showSpinningText = function() {
	this.game.add.tween(this.fSpinText.scale).to({
		x : 1,
		y : 1
	}, 500, Phaser.Easing.Back.Out, true);
};

Timer.prototype.updateLanguage = function() {
	this.fSpinText.text = getLang().TIME_STATE_SPIN;
	switch (this.lastState) {
	case 'BETTING':
		this.fState.text = getLang().TIME_STATE_BET;
		break;
	case 'WAITING':
		this.fState.text = getLang().TIME_STATE_WAIT;
		break;
	}
};