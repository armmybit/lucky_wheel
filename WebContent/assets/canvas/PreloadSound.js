// -- user code here --

/* --- start generated code --- */

// Generated by  1.5.4 (Phaser v2.6.2)


/**
 * PreloadSound.
 */
function PreloadSound() {
	
	Phaser.State.call(this);
	
}

/** @type Phaser.State */
var PreloadSound_proto = Object.create(Phaser.State.prototype);
PreloadSound.prototype = PreloadSound_proto;
PreloadSound.prototype.constructor = PreloadSound;

PreloadSound.prototype.init = function () {
	
	this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	this.scale.pageAlignHorizontally = true;
	this.scale.pageAlignVertically = true;
	
};

PreloadSound.prototype.preload = function () {
	
	this.load.pack('preload_sound', 'assets/pack.json');
	
	
	this.add.sprite(0.0, 0.0, 'bg');
	
	var _loading = this.add.text(640.0, 587.0, 'ที่นี้ครั้ง', {"font":"32px Kanit","fill":"#ffffff","align":"center"});
	_loading.alpha = 0.0;
	_loading.anchor.set(0.5, 0.5);
	
	var _loading_bar_frame = this.add.sprite(640.0, 640.0, 'loading_bar_frame');
	_loading_bar_frame.anchor.set(0.5, 0.5);
	
	var _loading_icon = this.add.sprite(462.5, 620.0, 'loading_bar_fill');
	
	var _btnStartGroup = this.add.group();
	_btnStartGroup.position.set(640.0, 360.0);
	_btnStartGroup.alpha = 0.0;
	
	var _btnStart = this.add.button(0.0, 0.0, 'btn_play', this.onStart, this, null, null, null, null, _btnStartGroup);
	_btnStart.anchor.set(0.5, 0.5);
	
	
	
	this.fLoading = _loading;
	this.fBtnStartGroup = _btnStartGroup;
	
	this.load.setPreloadSprite(_loading_icon, 0);
	
	this.load.onLoadStart.addOnce(this.startLoad, this);
	this.load.onLoadComplete.addOnce(this.loadComplete, this);
	
};

PreloadSound.prototype.create = function () {
	this.afterCreate();
	
};

/* --- end generated code --- */
// -- user code here --
PreloadSound.prototype.afterCreate = function() {
	// this.game.stage.disableVisibilityChange = true;
	// this.fBtnStartGroup.visible = false;
	// // console.log("PreloadSound");
	// this.delayTime = 1000;
	// this.fLoading.alpha = 0;
	// this.fLoading.text = this.game.loadingMessage2 + "...";
	// this.fLoading.position.x = 0 - (this.fLoading.width / 2);
	//
	// this.game.add.tween(this.fLoading).to({
	// alpha : 1
	// }, this.delayTime, Phaser.Easing.Cubic.Out, true, this.delayTime);
	//
	// this.game.add.tween(this.fLoading.position).to({
	// x : 640
	// }, this.delayTime, Phaser.Easing.Cubic.Out, true,
	// this.delayTime).onComplete.add(this.messageShow, this);

};

PreloadSound.prototype.startLoad = function() {
	this.game.stage.disableVisibilityChange = true;
	this.fLoading.text = this.game.loadingMessage2;

	this.fLoading.alpha = 1;
};

// PreloadSound.prototype.messageShow = function() {
// this.isMessageShow = true;
// this.hideMessage();
// };

PreloadSound.prototype.loadComplete = function() {
	// this.isLoadAssetComplete = true;
	// this.hideMessage();
	this.game.state.start("Level");
};

// PreloadSound.prototype.hideMessage = function() {
//
// if (this.isMessageShow == true && this.isLoadAssetComplete == true) {
// var target = 1280 + (this.fLoading.width);
// this.game.add.tween(this.fLoading.position).to({
// x : target
// }, this.delayTime, Phaser.Easing.Cubic.In, true,
// this.delayTime).onComplete.add(this.go, this);
// }
// };

// PreloadSound.prototype.go = function() {
// // this.fBtnStartGroup.renderable = true;
// // this.fBtnStartGroup.visible = true;
// // this.fBtnStartGroup.alpha = 1;
//
// // this.game.state.start("Level");
// };

// PreloadSound.prototype.onStart = function() {
// // this.game.state.start("Level");
// };
