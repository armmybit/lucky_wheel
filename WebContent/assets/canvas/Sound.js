// -- user code here --

/* --- start generated code --- */

// Generated by  1.5.4 (Phaser v2.6.2)


/**
 * Sound.
 */
function Sound() {
	
	Phaser.State.call(this);
	
}

/** @type Phaser.State */
var Sound_proto = Object.create(Phaser.State.prototype);
Sound.prototype = Sound_proto;
Sound.prototype.constructor = Sound;

Sound.prototype.init = function () {
	
	this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	this.scale.pageAlignHorizontally = true;
	this.scale.pageAlignVertically = true;
	
};

Sound.prototype.preload = function () {
	
	this.load.pack('sound', 'assets/pack.json');
	this.load.pack('boot', 'assets/pack.json');
	
};

Sound.prototype.create = function () {
	var _btnStart = this.add.button(947.0, 561.0, 'btn_start', this.onStart, this, null, null, null, null);
	
	
	
	// fields
	
	this.fBtnStart = _btnStart;
	this.afterCreate();
	
};

/* --- end generated code --- */
// -- user code here --
Sound.prototype.afterCreate = function() {
	// this.game.state.start("Level");
};

Sound.prototype.onStart = function() {
	this.game.state.start("Level");
};
