window.onload = function() {
	var game = new Phaser.Game(1280, 720, Phaser.AUTO);

	// Add the States your game has.
	game.state.add("Boot", Boot);
	game.state.add("PreloadSprite", PreloadSprite);
	game.state.add("PreloadSound", PreloadSound);
	// game.state.add("Sound", Sound);
	// game.state.add("Menu", Menu);
	// game.state.add("Preload", Preload);
	game.state.add("Level", Level);

	game.currentState = game.state.states['Level'];

	game.fadeOut = function(audio) {
		game.add.tween(audio).to({
			volume : 0.1
		}, 2000, Phaser.Easing.Cubic.Out, true);
	};

	game.fadeIn = function(audio) {
		game.add.tween(audio).to({
			volume : 1
		}, 2000, Phaser.Easing.Cubic.In, true);
	};

	// this.twMyBet = this.game.add.tween(this.fMyBetGroup.position).to({
	// y : targetPosY
	// }, 1000, Phaser.Easing.Cubic.Out, true, 0, -1, true);

	SoundManager.init(game);

	Game = game.state.states['Level'];
	State = game.state;
	IsConnect = false;
	IsLogin = false;

	// select loading messages
	var loadingMessages = getLang().LOADING_MESSAGES;
	var index = Math.floor(Math.random() * loadingMessages.length);
	game.loadingMessage1 = loadingMessages[index];
	loadingMessages.splice(index, 1);
	index = Math.floor(Math.random() * loadingMessages.length);
	game.loadingMessage2 = loadingMessages[index];

	game.state.start("Boot");

};
