/**
 * 
 */
// enum
var langEnum = {
	TH : 'TH',
	EN : 'EN'
};

var langKey = langEnum.TH;
if (localStorage.getItem('LANGUAGE') == null) {
	localStorage.setItem('LANGUAGE', langKey);
} else {
	langKey = localStorage.getItem('LANGUAGE');
}

var languageDict = {};
languageDict[langEnum.TH] = new LangTH();
languageDict[langEnum.EN] = new LangEN();

function getLang() {
	return languageDict[langKey];
}
