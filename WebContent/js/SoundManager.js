/**
 * 
 */

var sound = SoundManager;

function SoundManager() {
	this.bgm = null;
}

SoundManager.SFX = {
	BUTTON_1 : "button_1",
	BUTTON_BACK_1 : "button_back_1",
	BET : "bet",
	BEGIN_SPIN : "begin_spin",
	PLACE_CHIP : "place_chip",
	SELECT_CHIP : "select_chip",
	WIN : "win",
	WIN2 : "win2",
	WIN3 : "win3",
	SPIN : "spin1",
	WIN_FRAME_APPEAR : "win_frame_appear",
	WIN_BET : "win_bet",
	LOSE_BET : "lose_bet",
	APPEAR_3 : "appear_3",
	APPEAR_4 : "appear_4",
	APPEAR_5 : "appear_5",
	APPEAR_6 : "appear_6",
	APPEAR_7 : "appear_7",
	ADD_COIN : "add_coin",
	WIN_4 : "win_4",
	SPIN_1 : "spin_1",
	SPIN_2 : "spin_2",
	SPIN_3 : "spin_3",
	SPIN_4 : "spin_4",
	SPIN_5 : "spin_5",
	LOSE : "lose",
	LEVEL_UP_1 : "level_up_1",
	LEVEL_UP_2 : "level_up_2",
	FALLING_1 : "falling_1",
	BIG_WIN_1 : "big_win_1",
	BIG_WIN_2 : "big_win_2",
	NORMAL_WIN_1 : "normal_win_1",
	SCORING_1 : "scoring_1",
	SELECT_1 : "select_1",
	SELECT_2 : "select_2",
	GLOW_1 : "glow_1",
	SIZE_S : "size_s",
	SIZE_M : "size_m",
	SIZE_L : "size_l",
	ALERT_1 : "alert_1",
	CANCEL_1 : "cancel_1"

};

SoundManager.init = function(game) {
	this.game = game;
};

SoundManager.play = function(key) {
	this.game.add.audio(key).play();
};

SoundManager.playBGM = function() {
	if (this.bgm == null) {
		this.bgm = this.game.add.audio('bgm').play('', 0, 1, true);
	} else {
		this.bgm.stop();
		this.bgm = null;
		this.bgm = this.game.add.audio('bgm').play('', 0, 1, true);
	}
};

SoundManager.fadeOutBGM = function() {
	if (this.bgm != null) {
		// this.bgm.volume = 0;
		this.game.fadeOut(this.bgm);
	}
};

SoundManager.fadeInBGM = function() {
	if (this.bgm != null) {
		// this.bgm.volume = 1;
		this.game.fadeIn(this.bgm);
	}
};

// game.stopBGM = function(key) {
// game.lastBGM.stop();
// game.lastBGM = game.add.audio(key).stop();
// };
