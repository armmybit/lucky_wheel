var url = window.location.href;
var querystring = url.split("?")[1];
var key = querystring.split("=")[1];

var Client = {};

Client.socket = io.connect('https://angpao2.luckygame.in.th:14443/?key=' + key);
// Client.socket = io.connect('http://210.246.248.169:8986/?key=' + key);

var userInfo = {
	idx : '',
	gold : 0
};

// Client.isConnectServerError = false;

Client.socket.on('updatePayRates', function(data) {
	if (!IsLogin)
		return;

	if (data.code == 200) {
		var payRates = data.payRates;
		Game.setPayRates(payRates);
	}
});

Client.socket.on('connectResponse', function() {
	IsConnect = true;
	console.log("connect to server: " + IsConnect);
	Game.checkReconnect();
});

Client.login = function() {
	// Game.isConnectServer = true;

	// var idx = Math.floor(Math.random() * 10000);
	// var data = {
	// idx : idx,
	// gold : 100000
	// };
	Client.socket.emit('login');
};

Client.socket.on('loginResponse', function(data) {

	if (data.code == 200) {
		userInfo.idx = data.idx;
		userInfo.gold = data.gold;
		var payRates = data.payRates;
		var winRates = data.winRates;
		var trends = data.trends;

		Game.setTrends(trends);
		Game.setWinRates(winRates);
		Game.setPayRates(payRates);
		Game.setUserInfo(userInfo.idx, userInfo.gold);
		Game.onLoggedIn();

		IsLogin = true;
	} else {
		switch (data.code) {
		case 4000:// login fail
			Game.showMessage(getLang().ERROR_LOGIN, true);
			IsLogin = false;
			break;
		case 4001:// Logged in from another device
			Game.showMessage(getLang().ERROR_LOGIN_FROM_ANOTHER_DEVICE, true);
			IsLogin = false;
			break;
		}
	}
});

Client.bet = function(value, symbol) {
	var data = {
		idx : userInfo.idx,
		value : value,
		symbol : symbol
	};

	console.log('[bet] ' + JSON.stringify(data));
	Client.socket.emit('bet', data);
};

Client.socket.on('betResponse', function(data) {
	if (data.code == 200) {
		var gold = data.gold;
		Game.updateUserGold(gold);
		Game.followLastBet();
		
		userInfo.gold = gold;
		//this.fGoldPanel.deduct(this.lastSelectChip.value);
	} else {
		switch (data.code) {
		case 4010:// bet fail
			Game.showMessage(getLang().ERROR_BET_FAILED, false);
			Game.resumeBet();
			// Game.waitToRestart();
			break;
		case 4011:// Not enough gold
			Game.showMessage(getLang().ERROR_NOT_ENOUGH_GOLD, false);
			var gold = data.gold;
			console.log("Server[NOT ENOUGH GOLD] " + "current gold: "+ gold);
			Game.resumeBet();
			break;
		case 4012:// betting time out
			Game.showMessage(getLang().ERROR_BET_TIMES_UP, false);
			Game.resumeBet();
			break;
		default:// bet error
			Game.showMessage(getLang().ERROR_BET_FAILED, false);
			Game.resumeBet();
			// Game.waitToRestart();
			break;
		}
	}
});

Client.socket.on('leaderboard', function(data) {
	if (!IsLogin)
		return;

	var leaderboard = data.leaderboard;

	Game.setLeaderboard(leaderboard);

});

Client.socket.on('otherUserBet', function(data) {
	if (!IsLogin)
		return;

	// console.log('[otherUserBet] ' + JSON.stringify(data));

	var otherId = data.idx;
	var value = data.value;
	var symbol = data.symbol;

	if (otherId != userInfo.idx) {
		Game.otherUserBet(otherId, value, symbol);
	}
});

Client.socket.on('gameState', function(data) {
	if (!IsLogin)
		return;

	var state = data.state;
	var canBet = data.canBet;
	var time = data.time;

	Game.updateState(state, canBet, time);

});

Client.socket.on('result', function(data) {
	console.log(data);
	
	if (!IsLogin)
		return;

//	console.log(data);

	if (data.code == 200) {
		var index = data.index;
		var bigSymbol = data.bigSymbol;
		var smallSymbol = data.smallSymbol;
		var currentGold = data.currentGold;
		var win = data.win;
		var isBigWin = data.isBigWin;
		var winRates = data.winRates;
		var fullCircles = data.fullCircles;
		var extraAngle = data.extraAngle;
		var tweenType = data.tweenType;
		var trends = data.trends;

		Game.spin(index, bigSymbol, smallSymbol, currentGold, win, isBigWin, winRates, fullCircles, extraAngle, tweenType, trends);
		
		userInfo.gold = currentGold;
	} else {
		switch (data.code) {
		case 5000:// result fail
			Game.showMessage(getLang().ERROR_SERVER_DOWN, true);
			IsLogin = false;
			break;
		}
	}

});

Client.socket.on('serverResponse', function(data) {
	switch (data.code) {
	case 5001:// Server stoped
		Game.showMessage(getLang().ERROR_SERVER_STOP, true);
		IsLogin = false;
		break;
	}
});

Client.socket.on('connect', function(data) {
	// console.log('client connect');
});

Client.socket.on('disconnect', function(data) {

	IsConnect = false;
	IsLogin = false;

	Game.showMessage(getLang().CONNECTION_FAILED, false);
	Game.waitToRestart();
	// console.log('client disconnect');
	// good
});

Client.socket.on('connect_failed', function(data) {
	// console.log('client connect_failed');
});

Client.socket.on('connect_error', function(data) {
	// console.log('client connect_error');
	// good
});
