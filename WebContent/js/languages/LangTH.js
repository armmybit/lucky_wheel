/**
 * 
 */
function LangTH() {
	// SERVER ERROR
	this.ERROR_SERVER_DOWN = "มีการเข้าใช้งานเกินกำลังของระบบ!";
	this.ERROR_SERVER_STOP = "เซิฟเวอร์ ปิดปรับปรุง";

	// WIN PANEL
	this.WIN_PANEL_HEADER = "ชนะ";

	// Disconnected
	this.DISCONNECTED = "จบการเชื่อมต่อ";

	// Trying to reconnect
	this.RECONNECT = "กำลังเชื่อมต่อ...";

	// CONNECTION_FAILED
	this.CONNECTION_FAILED = "การเชื่อมต่อล้มเหลว";

	// NO BET RECORD
	this.REBET_NULL = "ไม่มีบันทึกเดิมพันรอบก่อนหน้า";

	// REBET
	this.REBET = "เดิมพันซำ";

	// LEADERBOARD PANEL
	this.LEADERBOARD_PANEL_HEADER = "ผลลัพธ์";

	// Client Bet
	this.ERROR_BET_STATE = "หมดเวลาเดิมพัน";
	this.ERROR_SELECT_CHIP = "กรุณาเลือกเหรียญ";
	this.ERROR_ENOUGH_GOLD = "จำนวนเงินไม่เพียงพอ";

	// ALERT
	this.ALERT_BETTING = "เริ่มเดิมพัน";
	this.ALERT_WAITING = "กรุณารอรอบต่อไป";
	this.ALERT_PREDICTING = "กำลังทำนายผล...";

	// TIME STATE
	this.TIME_STATE_BET = "เดิมพันได้";
	this.TIME_STATE_WAIT = "กรุณารอ";
	// this.TIME_STATE_SPIN = "กำลังหมุน...";
	this.TIME_STATE_SPIN = "กรุณารอผล";

	// HOW TO
	this.HOW_TO_HEADER = "วิธีการเล่น";
	this.HOW_TO_STEP_1 = "เลือกเหรียญเดิมพัน";
	this.HOW_TO_STEP_2 = "วางเดิมพันตามช่อง";
	this.HOW_TO_STEP_3 = "รับเงินรางวัล ตามผลลัพธ์";

	// PAY RATES
	this.PAY_RATES_HEADER = "อัตราการจ่าย";

	// LOSE MESSAGE
	this.LOSE_MESSAGE = "ขอให้โชคดี ในรอบถัดไป";
	this.LOSE_MESSAGE_2 = "ขอให้โชคดี ในรอบถัดไป";

	// WIN RATES PANEL
	this.WIN_RATES_PANEL_HEADER = "อัตราการชนะ";

	// GOLD PANEL
	this.GOLD_PANEL_HEADER = "คูปองทั้งหมด";

	// SETTINGS PANEL
	this.SETTINGS_PANEL_HEADER = "ภาษาไทย";

	// TREND
	this.TREND_HEADER_PANEL = "ประวัติการออกผลลัพธ์";
	this.TREND_LAST_NEW = "ใหม่";

	// PLAYER BAR
	this.PLAYER_BAR_REBET = "วางซ้ำ";
	this.PLAYER_BAR_OPTION = "ตัวเลือก";

	// RULES
	this.RULE_PANEL_HEADER = "กฎกติกา";
	this.RULE_SINGLE = "แทงเต็ง – แต้ม 1-6";
	this.RULE_SMALL_BIG = "สูง – แต้มระหว่าง 4-6\n" + "ต่ำ – แต้มระหว่าง 1-3";
	this.RULE_ODDS_SINGLE_PREFIX = "อัตรจ่าย 1 ต่อ ";
	this.RULE_ODDS_SINGLE_SUFFIX = "";
	this.RULE_ODDS_SMALL_BIG_PREFIX = "อัตรจ่าย 1 ต่อ ";
	this.RULE_ODDS_SMALL_BIG_SUFFIX = "";

	// OPTION PANEL
	this.OPTION_PANEL_HEADER = "ตัวเลือก";
	this.OPTION_PANEL_INFO_BTN = "กติกา";
	this.OPTION_PANEL_HOWTO_BTN = "วิธีการเล่น";
	this.OPTION_PANEL_SOUND_BTN = "เสียง";
	this.OPTION_PANEL_LANGUAGE_BTN = "ภาษา";
	this.OPTION_PANEL_EXIT_BTN = "ออกเกม";

	// GAME_RESULT
	this.GAME_RESULT_HEADER = "ผลลัพธ์";

	// ERROR PANEL
	this.ERROR_PANEL_HEADER = "ข้อความ";
	this.ERROR_PANEL_BTN = "ตกลง";

	// ERROR MESSAGE
	this.ERROR_BET_FAILED = "การเดิมพันล้มเหลว";
	this.ERROR_BET_TIMES_UP = "หมดเวลา";
	this.ERROR_NOT_ENOUGH_GOLD = "คูปองไม่เพียงพอ กรุณาเติมเงิน!!";
	this.ERROR_MLIVE_RESPONSE = "บัญชีผู้ใช้ไม่สามารถใช้งานได้ชั่วคราว\n" + "กรุณาเข้าสู่ระบบใหม่";
	this.ERROR_USER_NULL = "การเข้าสู่ระบบล้มเหลว\n" + "กรุณาลองใหม่อีกครั้ง";
	this.ERROR_SERVER = "ไม่สามารถเชื่อมต่อกับเซิฟเวอร์ได้";
	this.ERROR_PLACE_BET = "การเดิมพันล้มเหลว";
	this.ERROR_LOGIN = "การเข้าสู่ระบบล้มเหลว";
	this.ERROR_LOGIN_FROM_ANOTHER_DEVICE = "มีการเข้าสู่ระบบจากอุปกรณ์อื่น";

	// LOADING
	this.LOADING_MESSAGES = [ "ลาภกำลังลอยเข้ามาหาตัวคุณ", "วันนี้เงินทองคล่องมือ", "จะมีโชคลาภจากคนใกล้ชิดสนิทใจ", "โชคชะตา สามารถ พลิกผันได้ทุกเวลา", "ช่วงนี้ฟ้าเปิดมีโชคลาภ-ได้เงินก้อน",
			"จะได้โชคลาภจากสิ่งลี้ลับ", "ยิงตรงจาก ลาว มาเสี่ยงโชคที่ คำชะโนด ", "ที่นี่  “นักเสี่ยงโชค” เรียกมันว่า “สวรรค์”", "ที่นี่  “กำไร” มีมากกว่า “ขาดทุน”",
			"กล้าพอไหมที่จะมาเหยียบดินแดนแห่งนี้", "ดินแดนที่ว่ากันว่าเป็น “สวรรค์ของนักเดิมพัน”" ];

}
