/**
 * 
 */
function LangEN() {
	// SERVER ERROR
	this.ERROR_SERVER_DOWN = "Server Down";
	this.ERROR_SERVER_STOP = "Server Maintenance";

	// WIN PANEL
	this.WIN_PANEL_HEADER = "WIN";

	// Disconnected
	this.DISCONNECTED = "Disconnected";

	// Trying to reconnect
	this.RECONNECT = "Trying to reconnect";

	// CONNECTION_FAILED
	this.CONNECTION_FAILED = "Connection Failed";

	// NO BET RECORD
	this.REBET_NULL = "No previous round record of bets";

	// REBET
	this.REBET = "Rebet";

	// LEADERBOARD PANEL
	this.LEADERBOARD_PANEL_HEADER = "Result";

	// Client Bet
	this.ERROR_BET_STATE = "Can't bet this state";
	this.ERROR_BET_STATE_EX = "";
	this.ERROR_SELECT_CHIP = "Please select chip";
	this.ERROR_ENOUGH_GOLD = "Not enough gold";

	// ALERT
	this.ALERT_BETTING = "Betting time";
	this.ALERT_WAITING = "Please wait next round";
	this.ALERT_PREDICTING = "Predicting the future...";

	// TIME STATE
	this.TIME_STATE_BET = "Betting";
	this.TIME_STATE_WAIT = "Waiting";
	// this.TIME_STATE_SPIN = "Spinning...";
	this.TIME_STATE_SPIN = "Waiting...";

	// HOW TO
	this.HOW_TO_HEADER = "How to play";
	this.HOW_TO_STEP_1 = "Select your chips";
	this.HOW_TO_STEP_2 = "Place your bets on areas";
	this.HOW_TO_STEP_3 = "Collect your winnings!";

	// PAY RATES
	this.PAY_RATES_HEADER = "PAY RATES";

	// LOSE MESSAGE
	this.LOSE_MESSAGE = "BETTER LUCK NEXT TIME";
	this.LOSE_MESSAGE_2 = "BETTER LUCK NEXT TIME";

	// WIN RATES PANEL
	this.WIN_RATES_PANEL_HEADER = "Win Rates";

	// GOLD PANEL
	this.GOLD_PANEL_HEADER = "Total Coupon";

	// SETTINGS PANEL
	this.SETTINGS_PANEL_HEADER = "ENGLISH";

	// TREND
	this.TREND_HEADER_PANEL = "Trend";
	this.TREND_LAST_NEW = "NEW";

	// PLAYER BAR
	this.PLAYER_BAR_REBET = "REBET";
	this.PLAYER_BAR_OPTION = "OPTION";

	// RULES
	this.RULE_PANEL_HEADER = "Rules";
	this.RULE_SINGLE = "Single Dice Bet – The specific number 1-6";
	this.RULE_SMALL_BIG = "High – A number between 4-6\n" + "Low – A number between 1-3";
	this.RULE_ODDS_SINGLE_PREFIX = "Prize rates 1:";
	this.RULE_ODDS_SINGLE_SUFFIX = "";
	this.RULE_ODDS_SMALL_BIG_PREFIX = "Prize rates 1:";
	this.RULE_ODDS_SMALL_BIG_SUFFIX = "";

	// OPTION PANEL
	this.OPTION_PANEL_HEADER = "Option";
	this.OPTION_PANEL_INFO_BTN = "rules";
	this.OPTION_PANEL_HOWTO_BTN = "how to";
	this.OPTION_PANEL_SOUND_BTN = "sound";
	this.OPTION_PANEL_LANGUAGE_BTN = "language";
	this.OPTION_PANEL_EXIT_BTN = "exit";

	// GAME_RESULT
	this.GAME_RESULT_HEADER = "Results";

	// ERROR PANEL
	this.ERROR_PANEL_HEADER = "Message";
	this.ERROR_PANEL_BTN = "OK";

	// ERROR MESSAGE
	this.ERROR_BET_FAILED = "Bet failed";
	this.ERROR_BET_TIMES_UP = "TIME'S UP!";
	this.ERROR_NOT_ENOUGH_GOLD = "You don\'t have enough coupons, Please top up.";
	this.ERROR_MLIVE_RESPONSE = "Your account is temporarily unavailable.\n" + "try again later";
	this.ERROR_USER_NULL = "Login Failed\n" + "try again later";
	this.ERROR_SERVER = "Cannot connect to server";
	this.ERROR_PLACE_BET = "Place bet error";
	this.ERROR_LOGIN = "Login error";
	this.ERROR_LOGIN_FROM_ANOTHER_DEVICE = "Login from another device";

	// LOADING
	this.LOADING_MESSAGES = [ "Wealth is coming your way.", "Surely get windfall luck!", "Good cash flow today.", "Get windfall luck from the closed person.", "Luck prediction, Fortune telling.",
			"General life predictions", "Update windfall luck.", "Lately, Get windfall luck- Get huge amount of money ", "Get windfall luck, Obviously.", "Get windfall luck from the mystery.",
			"Straight from Laos to Kham Chanod to try luck." ];

}
